---?image=assets/img/gitlab_cover.png&size=70%

---
@snap[span-100]
# Gitlab CI-CD
@snapend

+++?image=assets/img/forrester-ci.svg&size=40% auto

+++?image=assets/img/gitlab_stages.gif&size=cover

+++?gist=hocinehacherouf/70468f19245a0ad546ec0a7726779420&lang=yml&title=.gitlab-ci.yml

@[1-3](Stages definition)
@[5-12](Test job)
@[14-22](Deployment on staging)
@[24-32](Deployment on production)

---

## Init(Coding4Fun);

+++

#### Step 1

Fork https://gitlab.com/hhocine/coding4fun-gitlab 

+++

#### Step 2

* Create a .gitlab-ci.yml
* Open it with gitlab IDE

+++

#### Step 3

@gist[yml zoom-10](hocinehacherouf/a06606ab91590b32eda8b88ba858f4f3)

@[1-2](build stage)
@[4-5](variables)
@[7-15](build job)

+++

#### Step 4

Check if your ci succeeded

+++

#### Step 5 (Heroku)

* Login into https://www.heroku.com/
* Create an app named eshop-{random number}

+++

####  Step 6

Add variables on Gitlab :

* $HEROKU_USER : Email used on Heroku
* $HEROKU_TOKEN : Token from Heroku

+++

#### Step 7

@gist[yml zoom-10](hocinehacherouf/bdb8d63448f0e711f8a2caf7da35e7ec)

@[1-3](add deploy stage)
@[18-29](add deploy job after build job)


